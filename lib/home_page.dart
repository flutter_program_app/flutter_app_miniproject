import 'dart:math';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_app_mini/const.dart';
import 'package:flutter_app_mini/util/my_button.dart';
import 'package:flutter_app_mini/util/result_message.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  // number pad list
  List<String> numberPad = [
    '7',
    '8',
    '9',
    'C',
    '4',
    '5',
    '6',
    'DEL',
    '1',
    '2',
    '3',
    '=',
    '0',
  ];

  // number A, number B
  bool isCountdown = false;
  int _start = 15;
  int _current = 15;
  late Timer _countdown;
  void resetCountdown() {
    _current = _start;
    isCountdown = false;
    _countdown.cancel();
  }

  void startCountdown() {
    if (isCountdown == false) {
      isCountdown = true;
      _countdown = Timer.periodic(Duration(seconds: 1), (timer) {
        setState(() {
          if (_current > 0) {
            _current--;
          } else {
            health = health - 1;
            _countdown.cancel();

            showDialog(
                context: context,
                builder: (context) {
                  return ResultMessage(
                    message: 'Time Up !',
                    onTap: goToNextQuestion,
                    icon: Icons.arrow_forward,
                  );
                });
          }
        });
      });
    }
  }

  int numberA = 1;
  int numberB = 1;
  int health = 3;
  int time = 13;
  int score = 0;

  void resetScore() {
    health = 3;
    time = 13;
    score = 0;
  }

  // user answer
  String userAnswer = '';
  // user tapped a button

  void buttonTapped(String button) {
    setState(() {
      if (button == '=') {
        // calculate if user is correct or incorrect
        checkResult();
      } else if (button == 'C') {
        // clear the input
        userAnswer = '';
      } else if (button == 'DEL') {
        // delete the last number
        if (userAnswer.isNotEmpty) {
          userAnswer = userAnswer.substring(0, userAnswer.length - 1);
        }
      } else if (userAnswer.length < 3) {
        // maximum of 3 numbers can be inputted
        userAnswer += button;
      }
    });
  }

  // check if user is correct or not
  void checkResult() {
    if (numberA + numberB == int.parse(userAnswer)) {
      score = score + 1;
      resetCountdown();
      isCountdown = true;
      showDialog(
          context: context,
          builder: (context) {
            return ResultMessage(
              message: 'Awesome, you are great!',
              onTap: goToNextQuestion,
              icon: Icons.arrow_forward,
            );
          });
    } else {
      health = health - 1;

      if (health == 0) {
        resetCountdown();
        isCountdown = true;
        resetScore();
        showDialog(
            context: context,
            builder: (context) {
              return ResultMessage(
                message: 'Game Over !!',
                onTap: goToNextQuestion,
                icon: Icons.rotate_left,
              );
            });
      } else {
        resetCountdown();
        isCountdown = true;

        showDialog(
            context: context,
            builder: (context) {
              return ResultMessage(
                message: 'Too bad, you got it wrong!',
                onTap: goToNextQuestion,
                icon: Icons.rotate_left,
              );
            });
      }
    }
  }

  // create random numbers
  var randomNumber = Random();

  // GO TO NEXT QUESTION
  void goToNextQuestion() {
    resetCountdown();
    startCountdown();
    // dismiss alert dialog
    Navigator.of(context).pop();

    // reset values
    setState(() {
      userAnswer = '';
    });

    // create a new question
    numberA = randomNumber.nextInt(100);
    numberB = randomNumber.nextInt(100);
  }

  // GO BACK TO QUESTION
  void goBackToQuestion() {
    // dismiss alert dialog
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    startCountdown();

    return Scaffold(
      backgroundColor: Color.fromARGB(255, 238, 216, 223),
      body: Column(
        children: [
          // level progress, player needs 5 correct answers in a row to proceed to next level
          Container(
              child: (Text(
                "HEALTH: $health \nTIME : $_current \nSCORE: $score",
                style: whiteTextStyle,
              )),
              height: 160,
              color: Color.fromARGB(255, 245, 150, 181)),

          // question
          Expanded(
            child: Container(
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // question
                    Text(
                      numberA.toString() + ' + ' + numberB.toString() + ' = ',
                      style: whiteTextStyle,
                    ),

                    // answer box
                    Container(
                      height: 50,
                      width: 100,
                      decoration: BoxDecoration(
                        color: Color.fromARGB(255, 255, 255, 255),
                        borderRadius: BorderRadius.circular(4),
                      ),
                      child: Center(
                        child: Text(
                          userAnswer,
                          style: whiteTextStyle,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),

          // number pad
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.all(4.0),
              child: GridView.builder(
                itemCount: numberPad.length,
                physics: const NeverScrollableScrollPhysics(),
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 4,
                ),
                itemBuilder: (context, index) {
                  return MyButton(
                    child: numberPad[index],
                    onTap: () => buttonTapped(numberPad[index]),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
