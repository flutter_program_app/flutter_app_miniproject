import 'package:flutter/material.dart';

var whiteTextStyle = const TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 32,
  color: Color.fromARGB(255, 0, 0, 0),
);
